#include <Arduino.h>

//Pines

int hall = 34;


//******Variables de velocimetro*****
 volatile float kilometraje = 0;

volatile float velocidad = 0.00;
volatile int cont = 0;
volatile int cont_vueltas = 0;

volatile unsigned timeLap = 0;
volatile unsigned long startTimeLap = 0;
volatile unsigned long endTimeLap = 0;



/***************
 *  Funcion de interrucion para obtener
 *  la velociad.
 * ***************/

void IRAM_ATTR isr_interruption(){
 

}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  pinMode(hall,INPUT); 
  attachInterrupt(digitalPinToInterrupt(hall), isr_interruption, RISING);

}

void loop() {
  // put your main code here, to run repeatedly:
}