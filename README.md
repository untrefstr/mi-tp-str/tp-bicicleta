# TP Velocímetro Bicicleta:
# - ALCANCE DEL PROYECTO  -
# Metas del proyecto:
    Desarrollo de un dispositivo, pequeño, 
    que obtenga, nuestre y guarde información sobre: 
    *_ Velocidad de una bicicleta.
    *_ Temperatura ambiente.
    *_ Ritmo cardíaco del ciclista.
    *_ Conexión por WiFi o Bluetooth.


# - Recursos:
    *_ Se utilizará Lenguaje C y/o C++
    *- Entorno Visual Studio Code con Platformio.
    *- Bibliotecas de arduino.
    *- 
    *_ Un desarrollador 10 hs semanales, durante 8 semanas.
    *_ Un placa esp32 devkit_v1
    *- Un módulo display oled 0,96" 128 x 64
    *_ Un sensor hall.
    *_ Un sensor de pulso cardiaco.
    *_ Un sensor de temperatura.
    *_ una bateria de 3,3 volt

# - Entregas:
    *- Cada 15 días.
    *- Primer entrega, 9-10-2021.
    *- Segunda entrega 23-10-2021.
    *- Tercer entrega  6-11-2021.
    *- Cuarta entrega  20-11-2021.
# - Fuera del alcance:
    *- Diseño y construcción 3d de gabinete.
    *- Montaje y despliegue.
    *- 
